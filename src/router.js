import { createRouter, createWebHistory } from 'vue-router';
import { auth } from './firebase';
import HomeView from './components/HomeView.vue'
import RegisterAccount from './components/RegisterAccount.vue'
import LoginAccount from './components/LoginAccount.vue'
import MainView from './components/MainView.vue'
import LeaderBoard from './components/LeaderBoard.vue'
import QuizCreate from './components/QuizCreate.vue'
import QuizStart from './components/QuizStart.vue'

const routes = [
  // Define your routes here
  { path: '/', name:"Home", component: HomeView },
  { path: '/register', name:"Register", component: RegisterAccount },
  { path: '/login', name:"LogIn", component: LoginAccount },
  { path: '/main', name:"Main", component: MainView, meta: {
    requiresAuth: true
  }},
  { path: '/leaderboard', name:"Leaderboard", component: LeaderBoard},
  { path: '/create-quiz', name:"Create a new Quiz", component: QuizCreate, meta: {
    requiresAuth: true
  }},
  { path: '/quiz', name:"Quiz Start", component: QuizStart, meta: {
    requiresAuth: true
  }},
  
  
  
];

const router = createRouter({
    history: createWebHistory(),
    routes
  });

  router.beforeEach((to, from, next) => {

    if (to.path === '/login' && auth.currentUser) {
      next('/main')
      return;
    }else if(to.path === '/register' && auth.currentUser){
      next('/main')
      return;
    }
    else if (to.matched.some(record => record.meta.requiresAuth) && !auth.currentUser) {
      next('/')
      return;
    }
    next();
  })

export default router;