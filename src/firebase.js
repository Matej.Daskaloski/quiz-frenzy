// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCHIrA_V_txMWqcYM6Jg_pAan9T0IvT18s",
  authDomain: "quiz-frenzy.firebaseapp.com",
  projectId: "quiz-frenzy",
  storageBucket: "quiz-frenzy.appspot.com",
  messagingSenderId: "443264259772",
  appId: "1:443264259772:web:3ad0a34374475c2f37a2b9",
  measurementId: "G-3JY7W8EGS2"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app)
const db = getFirestore(app);

export { auth, db};
